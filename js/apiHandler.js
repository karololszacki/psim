"use strict";

var api_server = "http://psim_backend.olszacki.pl/";
var api_connected = false;

if(Cookies.get("Auth") !== undefined && Cookies.get("Auth") !== ""){
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',  Cookies.get("Auth"));
        }
    });
    api_connected = true;
}

function api_login (username, password, success_callback, callback_param) {
    $.ajax({
        'url': api_server + "login",
        'data': JSON.stringify({"username": username, "password": password}),
        'type': 'POST',
        'processData': false,
        'contentType': 'application/json',
        'success': function(data, textStatus, request){
            success_callback(request, callback_param);
        }
    });
}

function api_login_default (old_api_query) {
    api_login("user", "user", default_login_success_callback, old_api_query);
}
function default_login_success_callback (request, old_api_query) {
    api_connected = true;

    Cookies.set("Auth", request.getResponseHeader('Authorization'));

    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization',  Cookies.get("Auth"));
        }
    });
    if(old_api_query !== undefined){
        old_api_query[0](old_api_query[1], old_api_query[2], old_api_query[3]);
    }
}

function api_get (some_uri, callback) {
    if (api_connected) {
        $.get(api_server + some_uri, callback);
    } else {
        api_login_default([api_get, some_uri, callback]);
    }
}


function api_post (some_uri, some_data, callback) {
    if (api_connected) {
        $.ajax({
            'url': api_server + some_uri,
            'data': JSON.stringify(some_data),
            'type': 'POST',
            'processData': false,
            'contentType': 'application/json',
            'error': function (jqXHR, textStatus, errorThrown){
                render_alert("danger", "API POST ERROR:", "status: " + textStatus + ", err: " + errorThrown);
                console.error(jqXHR);
            },
            'success': callback
        });
    } else {
        api_login_default([api_post, some_uri, some_data, callback]);
    }
}


function api_put (some_uri, some_data, callback) {
    if (api_connected) {
        $.ajax({
            'url': api_server + some_uri,
            'data': JSON.stringify(some_data),
            'type': 'PUT',
            'processData': false,
            'contentType': 'application/json',
            'error': function (jqXHR, textStatus, errorThrown){
                render_alert("danger", "API PUT ERROR:", "status: " + textStatus + ", err: " + errorThrown);
                console.error(jqXHR);
            },
            'success': callback
        });
    } else {
        api_login_default([api_post, some_uri, some_data, callback]);
    }
}


function api_delete (some_uri, callback) {
    if (api_connected) {
        $.ajax({
            'url': api_server + some_uri,
            'type': 'DELETE',
            'processData': false,
            'contentType': 'application/json',
            'error': function (jqXHR, textStatus, errorThrown){
                render_alert("danger", "API DELETE ERROR:", "status: " + textStatus + ", err: " + errorThrown);
                console.error(jqXHR);
            },
            'success': callback
        });
    } else {
        api_login_default([api_delete, some_uri, callback]);
    }
}
