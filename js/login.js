"use strict";

$("#app").on("submit", "#loginform", function(event) {
    event.preventDefault(); // prevent form submitting (keep it all SPA)
    Cookies.set("username", $("#username").val());
    login($("#username").val(), $("#password").val()); // call function that handles the form values
});


function login (username, password) {
    api_login(username, password, login_success_callback);
}

function login_success_callback (request) {
    default_login_success_callback(request);
    Cookies.set("logged_in", "true");
    render_default_layout();
    router.setRoute("/panel");
}


function is_loggedin () {
    // check cookie etc to determine if logged in
    if (Cookies.get("logged_in") === "true") {
        // !#TODO: auth test!
        $(".navbar-right").children().removeClass("hidden");
        $("#side-menu").removeClass("hidden");
        $("#login_form_link").addClass("hidden");
        return true;
    }
    return false;
}

function logout () {
    Cookies.remove("logged_in");
    Cookies.remove("Auth");
    render_default_layout();
    router.setRoute("/main");
}
