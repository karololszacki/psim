"use strict";

$("#app").on("submit", "#add_news", function(event) {
    event.preventDefault();

    api_post("news/add", {
        "date" : new Date($("#news_date").val()).toISOString(),
        "title" : $("#news_title").val(),
        "text": $("#news_content").val()
    }, add_news_callback);
});

function add_news_callback(data){
    console.log("ADD NEWS OK!");
    router.setRoute("/panel/news");
    console.log(data);
    render_alert("info", "Artykuł dodany!", "");
}


$("#app").on("submit", "#add_meal", function(event) {
    event.preventDefault();

    api_post("meal", {
        "description" : $("#meal_name").val(),
        "weight" : $("#meal_weight").val(),
        "price": $("#meal_price").val(),
        "mealType": $("#meal_type").val()
    }, add_menu_callback);
});

function add_menu_callback(data, textStatus, jqXHR){
    router.setRoute("/menu/" + data.id);
}


$("#app").on("submit", "#edit_meal", function(event) {
    event.preventDefault();

    api_put("meal", {
        "id" : $("#meal_id").val(),
        "description" : $("#meal_name").val(),
        "weight" : $("#meal_weight").val(),
        "price": $("#meal_price").val(),
        "mealType": $("#meal_type").val()
    }, edit_menu_callback);
});

function edit_menu_callback(data, textStatus, jqXHR){
    router.setRoute("/menu/" + data.id);
}

$("#app").on("click", ".delete_meal", function(event) {
    var temp_meal_ID = $(this).parent().parent()[0].id.replace("meal", "");
    api_delete("meal/" + temp_meal_ID, panel_menu);
});

var orderArr = [];
var orderPrice = 0;

$("#app").on("click", ".add_order", function(event) {
    var id = $(this).parent().parent()[0].id.replace("meal", "");
    var name = $(this).parent().parent().children().get(1).innerHTML;
    var price = parseFloat($(this).parent().parent().children().get(5).innerHTML);
    orderPrice += price;
    add_order(id, name, price);
    //api_delete("meal/" + temp_meal_ID, panel_menu);
});

function add_order(id, name, price){
    orderArr.push({id: id, name: name, one_price: price});
    updateOrdersTable();
}

$("#app").on("click", "#push_order", function(event) {
    router.setRoute("/order");
    $("#order-menu").addClass("hidden");
});


$("#app").on("submit", "#send_order", function(event) {
    event.preventDefault();
    var orderArrIds = [];
    for(var i = 0; i < orderArr.length; i++){
        orderArrIds.push(parseInt(orderArr[i].id));
    }

    api_post("order", {
        "contactNumber" : $("#order_phone").val(),
        "address" : $("#order_address").val(),
        "meals" : orderArrIds
    }, send_order_callback);
});

function send_order_callback(data, textStatus, jqXHR){
    router.setRoute("/menu");
    console.log(data);
    render_alert("info", "Zamówienie przyjęte!", "Numer zamówienia: X");
}
