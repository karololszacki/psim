"use strict";
// main.js: our renderer and router
// fetches and caches components
// launches actions defined in other files

var state = 0;
var componentsStore = store; // storejs localStorage wrapper, could be replaced with default localStorage & polyfill

var dataTableParams = {
    responsive: true,
    "language": {
        "url": "/js/vendor/datatables/dataTables.polish.lang"
    }
};

HandlebarsIntl.registerWith(Handlebars);

function render (dest, source, context) {
    var template = Handlebars.compile(source); // HandlebarsJS parsing of the template
    var html = template(context); // fill the template with context: replace {{ expressions }}
    $(dest).html(html); // insert to DOM with jQuery
}

function load (dest, source, context, callback) {
    // NEVER CACHE
    if (true || componentsStore.get(source) === undefined) { // if component not in cache store (localStorage)
        $.get("/components/" + source + ".html", function ok (data) { // grab the component
            componentsStore.set(source, data); // add to cache
            render(dest, data, context); // render it from the cache
            if(callback){ callback(); }
        });
    } else { // if it is in the cache, render it from there
        render(dest, componentsStore.get(source), context);
        if(callback){ callback(); }
    }
}

function render_dummy (data) {
    load("#content", "dummy", {data: data});
}
function render_alert(type, title, data){
    load("#status", "alert", {type: type, title: title, data: data});
}
function clear_alert(){
    $("#status").html("");
}
function render_modal(title, data, id){
    load("#status", "modal", {title: title, data: data, id: id}, modal_callback);
}
function clear_modal(){
    $("#status").html("");
}
function modal_callback(){
    $('button[data-dismiss="modal"]').off();
    $('button[data-dismiss="modal"]').on("click", clear_modal);
}
function render_default_layout () {
    load("#nav", "nav", {username: Cookies.get("username")}, allroutes);
    load("#footer", "footer");
}

render_default_layout();


function appstart () {
    load("#content", "sites/login");
}

function appclose () {
    logout();
    cacheclear(); // clear cache on logout, weird thing to do...
    state = 1;
}

function render_splash(){
    load("#content", "sites/splash");
}
function render_news(data){
    load("#content", "sites/news", {news: data});
}
function render_menu(data){
    var food = [];
    var drink = [];
    var vegan = [];

    for(var i = 0; i < data.length; i++){
        if (data[i]["mealType"].toLowerCase() === "food"){
            food.push(data[i]);
        }
        if (data[i]["mealType"].toLowerCase() === "drink"){
            drink.push(data[i]);
        }
        if (data[i]["mealType"].toLowerCase() === "vegan"){
            vegan.push(data[i]);
        }
    }
    load("#content", "sites/menu", {food: food, drink:drink, vegan:vegan}, menu_callback_scroll);
}
function render_special(data){
    load("#content", "sites/special", {special: data});
}
function render_contact(){
    load("#content", "sites/contact");
}

function render_panel_news(data){
    load("#content", "sites/panel/news", {news: data}, panel_news_cb);
}
function panel_news_cb(){
    $('#news-tabela').DataTable(dataTableParams);
}

function render_panel_menu(data){
    load("#content", "sites/panel/menu", {data: data}, panel_menu_cb);
}

function panel_menu_cb(){
    $('#menu-tabela').DataTable(dataTableParams);
}

function render_panel_orders(data){
    load("#content", "sites/panel/orders", {data: data}, panel_orders_cb);
}
function panel_orders_cb(){
    $('#orders-tabela').DataTable(dataTableParams);
}

function panel_add_news(){
    load("#content", "sites/panel/add/news", null, setDateFieldToNow);
}
function panel_add_menu(){
    load("#content", "sites/panel/add/menu");
}

function setDateFieldToNow(){
    document.getElementsByClassName('datepicker')[0].value = new Date().toISOString();
}

function updateOrdersTable(){
    $("#order-menu").removeClass("hidden");
    load("#order-menu", "order", {data: orderArr, price: orderPrice});
}
function render_order(){
    load("#content", "sites/take_order", {data: orderArr, price: orderPrice});
}

// functions that are called by the router when uri paths are matched
// basically each of them loads a specific component

function main () {
    render_splash();
}
function news () {
    api_get("news", render_news);
}
var mealID = null;
function menu (path) {
    console.log("Meal path: " + path);
    if(path !== ""){
        mealID = path;
    }else{
        mealID = null;
    }
    api_get("meal", render_menu);
}
function menu_callback_scroll() {
    if (mealID !== null) {
        window.scrollTo(0, $("#meal" + mealID).offset().top - $("#app").offset().top);
        $("#meal" + mealID).addClass("activeMeal");
    }
}
function special () {
    api_get("special", render_special);
}


function panel () {
    if (is_loggedin()) {
        // load("#content", "sites/panel");
        router.setRoute("/panel/news");
    } else {
        router.setRoute("/main");
    }
}

function panel_news(){
    api_get("news", render_panel_news);
}
function panel_menu(){
    api_get("meal", render_panel_menu);
}
function panel_orders(){
    api_get("order", render_panel_orders);
}
function panel_edit_news(){
    // load("#content", "sites/panel/edit/news");
}
function panel_edit_menu(path){
    api_get("meal/" + path, render_panel_edit_menu);
}
function render_panel_edit_menu(param){
    load("#content", "sites/panel/edit/menu", param, function(){ $('#meal_type').val(param.mealType); });
}

function cacheclear () {
    componentsStore.clear();
}


// all of the routes utilising above functions must be specified here
var routes = {
    '/': main,
    '/main': main,
    '/news': news,
    '/menu/?((\w|.)*)': menu,
    '/special': special,
    '/order': render_order,
    '/contact': render_contact,
    '/panel': {
        '/add': {
            '/news': {
                on: panel_add_news
            },
            '/menu': {
                on: panel_add_menu
            }
        },
        '/edit': {
            '/news/?((\w|.)*)': {
                on: panel_edit_news
            },
            '/menu/?((\w|.)*)': {
                on: panel_edit_menu
            }
        },
        '/news': {
            on: panel_news
        },
        '/menu': {
            on: panel_menu
        },
        '/orders': {
            on: panel_orders
        },
        on: panel
    },
    '/login': appstart,
    '/logout': appclose,
    '/clear': cacheclear
};

var router = tarantino.Router(routes);

router.configure({html5history: true, on: allroutes, notfound: routenotfound, convert_hash_in_init: false});
// use History API (we'll have nice "real" URIs), call func allroutes for every route change detected

// router initialisation - important! it won't work without it
// we render either the welcome screen ("login page") or the "panel"
//var loc = document.location.href.replace(document.location.origin, "");
function loc(){
    return document.location.href.replace(document.location.origin, "");
}

if (loc() != "" && loc() !== "/") {
    router.init(loc());
} else {
    router.init("/main");
}

function allroutes () {
    // update navmenu links: mark rendered route (the selected nav component, eg news) as an active tab
    //loc = document.location.href.replace(document.location.origin, "");
    $("#navmenu").children().children().children().removeClass("active").find('a[href="' + loc() + '"]').parent().addClass("active");
    is_loggedin();
    // cacheclear();
}

function routenotfound(){
    console.log("Route not found! Route: " + loc());
    render_alert("danger", "Route not found!", "Route: " + loc());
    if($("#content").text() === ""){
        router.setRoute("/main");
    }
}

// router helper: catches all link clicks, stops the browser from going there and updates the route (and renders the module) instead
// this is how the SPA (single page application) effect is achieved
$("#app").on("click", "a", function(event) {
    event.preventDefault();
    router.setRoute(this.href.replace(document.location.origin, ""));
});
