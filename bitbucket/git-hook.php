<?php

    ignore_user_abort(true);
    set_time_limit(0);

    $subdomain = "psim";

    if ($_GET['secret'] != 'gc2WixRl9LVRwP1gc2WixRl9LVRwP1') {
        die("fail");
    }

    header('Content-Type: text/plain');
    putenv('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin');

    $branch = "deploy";
    if ($_GET['branch'] == "master") {
        $branch = "master";
    }

    exec("git checkout " . $branch, $out1);
    exec("git pull 2>&1", $out2);

    // start magic
    ob_start();
    echo "success";
    header('Connection: close');
    header('Content-Length: ' . ob_get_length());
    ob_end_flush();
    ob_flush();
    flush();
    // end magic, stuff is running in the bg but response was already sent to the browser

    //exec("../../builders/" . $subdomain . ".sh", $out);
    file_put_contents("/var/log/olszacki/build.log", "-- " . date("Y-m-d H:i:s") . " --\n" . print_r($out2, true) . "\n---------\n", FILE_APPEND);
    //print_r($out5);
    //exec("ls -al ../", $out6);
    //print_r($out6);


    require_once 'cloudflare.class.php';

    $cloudflare = new cloudflare_api("karololszak@kozacki.pl", "924000b7e9b167d52a03abdcc03f1a93a0ebb");

    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/");
    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/index.html");
    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/js/apiHandler.js");
    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/js/logic.js");
    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/js/login.js");
    $cloudflare->zone_file_purge("olszacki.pl", "http://" . $subdomain . ".olszacki.pl/js/main.js");

    die();
